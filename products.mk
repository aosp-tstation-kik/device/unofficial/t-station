# here we can use LOCAL_PATH ( not LOCAL_DIR ).

##########################
# packages
##########################
PRODUCT_PACKAGES += \
		ContactsdProvider \
		DownloadProvider \
		HTMLViewer \
		Launcher3 \
    FusedLocation \
    InputDevices \
    Keyguard \
    Settings \
    SystemUI \
    apache-xml \
    bouncycastle \
		busybox \
    cacerts \
    com.android.future.usb.accessory \
    conscrypt \
    core \
    core-junit \
    dalvikvm \
    dexdeps \
    dexdump \
    dexlist \
    dexopt \
    dmtracedump \
    dx \
    ext \
    hostapd \
    hprof-conv \
    libcrypto \
    libdvm \
    libexpat \
    libicui18n \
    libicuuc \
    libjavacore \
    libnativehelper \
    librs_jni \
    libssl \
    libvideoeditor_core \
    libvideoeditor_jni \
    libvideoeditor_osal \
    libvideoeditorplayer \
    libvideoeditor_videofilters \
    libz \
    lint \
		libion \
    local_time.default \
    network \
    okhttp \
    pand \
    power.default \
    sdptool \
    netd \
    keystore 

PRODUCT_PACKAGES += \
		wpa_supplicant \
    wpa_supplicant.conf \
    dhcpcd.conf \
#		wpa_supplicant_overlay.conf \
#		p2p_supplicant_overlay.conf


PRODUCT_PACKAGES += \
		OpenWnn \
    libwnndict \
    libWnnEngDic \
    libWnnJpnDic 


PRODUCT_PACKAGES += \
    audio \
		audio.usb.default \
    audio_policy.default \
    audio.primary.default \
		audio.r_submix.default \
		libaudio-resampler \
		libdownmix \
		libtinyalsa 



# TARGET_BOARD_HARDWARE in BoardConfig.mk must be 'rk30board'
PRODUCT_PACKAGES += \
		gralloc.rk30board \
		gralloc.rk30board_firefly \
		hwcomposer.rk30board \
		hwcomposer.rk30board_firefly

# test program for system/core/libion
PRODUCT_PACKAGES += \
		iontest

# drm
PRODUCT_PACKAGES += \
		drmservice 

# uboot
PRODUCT_PACKAGES += \
		u-boot \
		u-boot-loader

# hw codecs
PRODUCT_PACKAGES += \
		libvpu

#		libstagefrighthw


#-------------------------
# inherit
#-------------------------
$(call inherit-product, build/target/product/core_base.mk)
$(call inherit-product-if-exists, frameworks/webview/chromium/chromium.mk)
$(call inherit-product-if-exists, frameworks/base/data/keyboards/keyboards.mk)
$(call inherit-product-if-exists, frameworks/base/data/fonts/fonts.mk)
$(call inherit-product-if-exists, frameworks/base/data/sounds/AudioPackage5.mk)

##########################
# copy files
##########################
PRODUCT_COPY_FILES += \
    frameworks/av/media/libeffects/data/audio_effects.conf:system/etc/audio_effects.conf \
    $(LOCAL_PATH)/copy-files/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    $(LOCAL_PATH)/copy-files/etc/p2p_supplicant_overlay.conf:system/etc/wifi/p2p_supplicant_overlay.conf \
    $(LOCAL_PATH)/copy-files/etc/wifi_efuse_8189e.map:system/etc/wifi/wifi_efuse_8189e.map \
    $(LOCAL_PATH)/copy-files/etc/wpa_supplicant_overlay.conf:system/etc/wifi/wpa_supplicant_overlay.conf \
    $(LOCAL_PATH)/copy-files/etc/asound.conf:system/etc/asound.conf \
    $(LOCAL_PATH)/copy-files/etc/audio_policy.conf:system/etc/audio_policy.conf \
    $(LOCAL_PATH)/copy-files/etc/media_codecs.xml:system/etc/media_codecs.xml \
    $(LOCAL_PATH)/copy-files/etc/media_profiles_default.xml:system/etc/media_profiles_default.xml 


# root directory
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/root/fstab.rk30board.bootmode.emmc:root/fstab.rk30board.bootmode.emmc \
    $(LOCAL_PATH)/copy-files/root/fstab.rk30board.bootmode.unknown:root/fstab.rk30board.bootmode.unknown \
    $(LOCAL_PATH)/copy-files/root/init.connectivity.rc:root/init.connectivity.rc \
    $(LOCAL_PATH)/copy-files/root/init.rc:root/init.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.bootmode.emmc.rc:root/init.rk30board.bootmode.emmc.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.bootmode.unknown.rc:root/init.rk30board.bootmode.unknown.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.environment.rc:root/init.rk30board.environment.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.rc:root/init.rk30board.rc \
    $(LOCAL_PATH)/copy-files/root/init.rk30board.usb.rc:root/init.rk30board.usb.rc \
    $(LOCAL_PATH)/copy-files/root/ueventd.rk30board.rc:root/ueventd.rk30board.rc


# install scripts
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/install-scripts/install-boot.sh:install-scripts/install-boot.sh  \
    $(LOCAL_PATH)/copy-files/install-scripts/install-gapp.sh:install-scripts/install-gapp.sh  \
    $(LOCAL_PATH)/copy-files/install-scripts/install-system.sh:install-scripts/install-system.sh


# install tools
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/copy-files/install-tools/AndroidTool_Release_v2.52.zip:install-tools/AndroidTool_Release_v2.52.zip  \
    $(LOCAL_PATH)/copy-files/install-tools/DriverAssitant_v4.6.zip:install-tools/DriverAssitant_v4.6.zip        \
    $(LOCAL_PATH)/copy-files/install-tools/Linux_Upgrade_Tool_1.34.zip:install-tools/Linux_Upgrade_Tool_1.34.zip    \
    $(LOCAL_PATH)/copy-files/install-tools/RK3126MiniLoaderAll_V2.31.bin:install-tools/RK3126MiniLoaderAll_V2.31.bin  \
    $(LOCAL_PATH)/copy-files/install-tools/readme.txt:install-tools/readme.txt


##########################
# property overrides
##########################
# "PRODUCT_PROPERTY_OVERRIDES += xxx" are moved to ./system.prop
