# Copyright (C) 2012 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# here we can use LOCAL_PATH ( not LOCAL_DIR ).


$(call inherit-product, $(LOCAL_PATH)/products.mk)

PRODUCT_NAME := t_station
PRODUCT_DEVICE := t-station
PRODUCT_BRAND := rockchip
PRODUCT_MODEL := TS-1-0
PRODUCT_BOARD := rk30sdk
PRODUCT_MANUFACTURER := rockchip


PRODUCT_CHARACTERISTICS := tablet,nosdcard
PRODUCT_AAPT_CONFIG := normal ldpi mdpi hdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi
PRODUCT_LOCALES := ja_JP en_US


# default is nosdcard, S/W button enabled in resource
DEVICE_PACKAGE_OVERLAYS := $(LOCAL_PATH)/overlay
