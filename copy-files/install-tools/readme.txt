This directory contains rochchip's tools to read/write nand flash memory:

 * androidtool & driver assistant
    AndroidTool_Release_v2.52.zip
    DriverAssitant_v4.6.zip

    download site:
		    http://opensource.rock-chips.com/wiki_AndroidTool
		    https://github.com/rockchip-linux/tools/tree/rk3399/windows


 * linux upgrade tool
    Linux_Upgrade_Tool_1.34.zip

		download site: http://www.t-firefly.com/doc/download/31.html#linux_12 


 * boot loader
   RK3126MiniLoaderAll_V2.31.bin ( contained in For_RK3126_devices.rar )

   download site: https://forum.xda-developers.com/android/general/rk-rockchip-bootloader-collection-t3739510 
   ( note: this is not officially distributed, use it at your own risk. )


