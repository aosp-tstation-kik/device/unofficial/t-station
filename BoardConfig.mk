# config.mk
#
# Product-specific compile-time definitions.
#

TARGET_NO_BOOTLOADER := true
TARGET_NO_KERNEL := true

TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a9
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_SMP := true

SMALLER_FONT_FOOTPRINT := true
MINIMAL_FONT_FOOTPRINT := true
# Some framework code requires this to enable BT
#@@@@BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH := false
#@@@@BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/generic/common/bluetooth

# rockchip
TARGET_BOARD_HARDWARE := rk30board
TARGET_BOARD_PLATFORM := rk312x
TARGET_BOARD_PLATFORM_GPU := mali400
TARGET_DEVICE := rk312x
TARGET_BOOTLOADER_BOARD_NAME := rk30sdk
TARGET_BOARD_PLATFORM_PRODUCT := tablet
BOARD_WITH_IOMMU := true

# hw/gralloc, hw/hwcomposer
GRAPHIC_MEMORY_PROVIDER := dma_buf
BOARD_USE_LCDC_COMPOSER := true
BOARD_LCDC_COMPOSER_LANDSCAPE_ONLY := false
#...currently this is only used in hwcomposer
TARGET_BOARD_OPTIMIZE := t_station

# framework 
USE_OPENGL_RENDERER := true
#...this may not to be used.
BOARD_USE_LEGACY_UI := true

# wifi
BOARD_WLAN_DEVICE := realtek
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION := VER_0_8_X
WIFI_DRIVER_SOCKET_IFACE := wlan0
#...this library is build at hardware/$(BOARD_WLAN_DEVICE) and
#...used to build external/wpa_supplicant_8
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)

# surfaceflinger setting
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := true
HWCOMPOSER_WITH_INTEGER_RECT := true

# root directory
#...use own init.rc
TARGET_PROVIDES_INIT_RC := true
